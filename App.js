import React from 'react';
import ReactDOM from 'react-dom';




// наполнение страницы
var Wrap = React.createClass({
    render: function() {
        return(
            <div className="Wrap">
                <div className="header">
                    <img className="head" src="img/Dropbox-logo.png" alt="dbox"/>
                    <p className="head inp"><b>Access token:</b></p>
                    <input type="text" size="50" className="head inp"/>
                    <button id="confirm" className="head inp">Confirm</button>
                </div>

                <div className="btns">
                    <button className="btn"></button>
                    <button className="btn"></button>
                    <button className="btn"></button>
                    <button className="btn"></button>
                    <button className="btn"></button>
                </div>

                <div className="content">
                    <table>
                        <thead>
                            <tr>
                                <th>Checked</th>
                                <th>Name</th>
                                <th>Size</th>
                                <th>Modification date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>some folder</td>
                                <td>-</td>
                                <td>2017-03-07</td>
                             </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>ThisWarofMine.wav</td>
                                <td>156</td>
                                <td>2017-03-07</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>Tuned To Live.wav</td>
                                <td>755</td>
                                <td>2017-05-07</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>123.txt</td>
                                <td>14</td>
                                <td>2017-03-07</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>notes.txt</td>
                                <td>65</td>
                                <td>2017-09-05</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>rules.txt</td>
                                <td>54</td>
                                <td>2017-02-01</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>somepic.png</td>
                                <td>887</td>
                                <td>2017-11-01</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            );
    }
});

ReactDOM.render(
    <Wrap />,
    document.getElementById('root')
);
