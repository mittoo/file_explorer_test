var path = require('path');
var webpack = require('webpack');

// создание файла bundle.js и перевод в него кода из App.js
module.exports = {
	entry: './App.js',
	output: {
		path: __dirname + '/public',
		filename: 'bundle.js'
	},
	watch: true,
	module: {
		loaders: [
			{
				test: /.jsx?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['es2015', 'react']
				}
			}
		]
	},
	resolve: {
    	extensions: ['.js', '.json']
  }
};